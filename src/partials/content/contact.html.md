## Support

Free support is provided through [StackOverflow](http://stackoverflow.com/). Paid support is provided through [my company](http://bevry.me/support).

## Consulting

Consulting services are available through [my company](http://bevry.me/services).

## Contact

For all other purposes, my details are:

- [personal email](mailto:b@lupton.cc)
- [work email](mailto:b@bevry.me)
- [skype](skype:balupton?add)